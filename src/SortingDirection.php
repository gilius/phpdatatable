<?php

namespace Datatable;

enum SortingDirection: string {
    case Asc = "asc";
    case Desc = "desc";


    public function invertIf(bool $invert): SortingDirection {
        if (!$invert) {
            return $this;
        }

        //Invert
        return $this === self::Desc ? self::Asc : self::Desc;
    }
}