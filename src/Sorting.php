<?php

namespace Datatable;

use Doctrine\ORM\QueryBuilder;
use Json\JsonField;

class Sorting {
    #[JsonField]
    public string $field = "";
    #[JsonField]
    public SortingDirection $direction = SortingDirection::Asc;


    public function appendQuery(QueryBuilder $qb, Config $config): void {
        foreach ($config->getQuerySortingField($this->field) as $sortingConfig) {
            $direction = $sortingConfig->forcedDirection ?: $this->direction->invertIf($sortingConfig->inverted);
            $qb->addOrderBy($sortingConfig->field, $direction->value);
        }
    }
}