<?php


namespace Datatable;


class ResultClass {

    public string $resultClass;
    public string $alias;
    public string $group;

    /**
     * @param string $resultClass
     * @param string $alias
     * @param string $group
     */
    public function __construct(string $resultClass, string $alias, string $group) {
        $this->resultClass = $resultClass;
        $this->alias = $alias;
        $this->group = $group;
    }
}