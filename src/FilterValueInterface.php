<?php


namespace Datatable;


use Doctrine\ORM\Query\Expr\Base;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\Query\Expr\Func;
use Doctrine\ORM\QueryBuilder;

interface FilterValueInterface {
    function getQueryComparison(string $field, QueryBuilder $qb, Config $config): Comparison|Func|Base|string|null;

    function isHaving(string $field): bool;
}