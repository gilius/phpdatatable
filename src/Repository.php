<?php


namespace Datatable;

use Datatable\Exceptions\RepositoryException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Exception;

class Repository {
    private EntityManager $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param Config $config
     * @return int
     * @throws ORMException
     * @api
     */
    public function getCount(Request $request, Config $config): int {
        $paginator = $this->getQuery($request, $config, false);
        return $paginator->count();
    }


    /**
     * @param Request $request
     * @param Config $config
     * @param bool $listRequest
     * @return Paginator
     * @throws ORMException
     */
    private function getQuery(Request $request, Config $config, bool $listRequest): Paginator {
        $resultClass = $config->getResultClass();
        $repo = $this->em->getRepository($resultClass->resultClass);

        $qb = $repo->createQueryBuilder($resultClass->alias);
        $qb->groupBy($config->getResultClass()->group);

        $addJoinsFn = $config->getAddJoinsFn();
        if ($addJoinsFn) {
            $addJoinsFn($qb);
        }

        $andWhereList = [];
        $andHavingList = [];

        foreach ($config->getInitialFilters() as $filter) {
            $comparison = $filter->getQueryComparison($qb, $config, false);
            if ($comparison) {
                if ($filter->isHaving($config, false)) {
                    $andHavingList[] = $comparison;
                } else {
                    $andWhereList[] = $comparison;
                }
            }
        }


        foreach ($request->filters as $filter) {
            $comparison = $filter->getQueryComparison($qb, $config);
            if ($comparison) {
                if ($filter->isHaving($config)) {
                    $andHavingList[] = $comparison;
                } else {
                    $andWhereList[] = $comparison;
                }
            }
        }


        //Global query
        if ($config->getGlobalQueryFields() && $request->query) {
            $items = [];
            foreach ($config->getGlobalQueryFields() as $field) {
                $items[] = $qb->expr()->like($field, ':query');
            }
            $andWhereList[] = $qb->expr()->orX(...$items);
            $qb->setParameter('query', "%$request->query%");
        }

        //Connect all conditions
        if ($andWhereList) {
            $qb->where($qb->expr()->andX(...$andWhereList));
        }
        if ($andHavingList) {
            $qb->having($qb->expr()->andX(...$andHavingList));
        }

        if ($listRequest) {
            //Sorting
            foreach ($request->sorting as $item) {
                $item->appendQuery($qb, $config);
            }

            //Pagination
            $qb->setFirstResult($request->getFirstResult());
            $qb->setMaxResults($request->getMaxResults());
        }

        $query = $qb->getQuery();

        return new Paginator($query, false);
    }

    /**
     * @throws RepositoryException
     * @throws ORMException
     * @api
     */
    public function getList(Request $request, Config $config): array {
        $paginator = $this->getQuery($request, $config, true);

        try {
            $fn = $config->getFormatDataFn();
            $items = $paginator->getIterator()->getArrayCopy();
            $handleDataFn = $config->getHandleDataFn();
            $handleDataFn && $handleDataFn($items);
            $result = [];
            $getChildrenFn = $config->getGetChildrenFn();
            foreach ($items as $item) {
                $result[] = $fn ? $fn($item) : $item;
                $children = $getChildrenFn ? $getChildrenFn($item) : [];
                foreach ($children as $child) {
                    $result[] = $child;
                }
            }

            return $result;
        } catch (Exception $e) {
            throw new RepositoryException($e->getMessage(), $e->getCode(), $e);
        }
    }
}