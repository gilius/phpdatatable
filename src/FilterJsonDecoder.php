<?php


namespace Datatable;


use Datatable\Exceptions\FilterTypeNotImplemented;
use Json\InterfaceDecoder;
use Json\Json;
use Json\JsonDecodeException;
use Json\JsonDecoderInterface;
use Json\StaticTypeGetter;
use ReflectionException;

class FilterJsonDecoder implements JsonDecoderInterface {
    function encode(mixed $value): mixed {
        try {
            $encoded = Json::prepareObjectEncoding($value, Filter::class);
            $encoded->data = Json::prepareObjectEncoding($value->data, get_class($value->data));
            return $encoded;
        } catch (ReflectionException) {
            return null;
        }
    }

    /**
     * @throws FilterTypeNotImplemented
     * @throws JsonDecodeException
     */
    function decode(mixed $value): Filter {
        $type = $value->type ?? null;
        $decoded = Json::decodeObject($value, Filter::class);
        $decoder = new InterfaceDecoder(new StaticTypeGetter($type), FilterType::class);

        /** @var FilterValueInterface|null $typeData */
        $typeData = $decoder->decode($value->data);

        if ($typeData === null) {
            throw new FilterTypeNotImplemented($type);
        }
        $decoded->data = $typeData;

        return $decoded;
    }
}