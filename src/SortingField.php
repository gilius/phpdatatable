<?php

namespace Datatable;

use Json\JsonField;

class SortingField {
    /** @var SortingConfig[] $inQuery */
    public array $configs;
    #[JsonField]
    public string $inApi;

    /**
     * @param SortingConfig|SortingConfig[]|string $configs
     * @param string $inApi
     */
    public function __construct(SortingConfig|array|string $configs, string $inApi) {
        $this->configs = $this->prepare($configs);
        $this->inApi = $inApi;
    }

    /**
     * @param SortingConfig|SortingConfig[]|string $configs
     * @return SortingConfig[]
     */
    private function prepare(SortingConfig|array|string $configs): array {
        if (is_string($configs)) {
            return [new SortingConfig($configs)];
        }
        if ($configs instanceof SortingConfig) {
            return [$configs];
        }
        return $configs;
    }
}