<?php

namespace Datatable;

use Closure;
use Datatable\Exceptions\InvalidConfigException;
use Json\JsonField;
use Json\JsonObjectArray;

class Config {
    /** @var Field[] */
    #[JsonObjectArray(className: Field::class, decoder: new FieldDecoder())]
    public array $allowedFilterFields;
    /** @var SortingField[] */
    #[JsonObjectArray(className: SortingField::class, decoder: new FieldDecoder())]
    public array $allowedSortingFields;
    /** @var Field[] */
    #[JsonField]
    public array $globalQueryFields;
    private ResultClass $resultClass;
    /** @var string[] */
    private array $initialFilters;

    private ?Closure $formatDataFn;

    private ?Closure $getChildrenFn;

    private ?Closure $addJoinsFn;
    private ?Closure $handleDataFn;

    /**
     * @param ResultClass $resultClass
     * @param Field[] $allowedFilterFields
     * @param SortingField[] $allowedSortingFields
     * @param string[] $globalQueryFields
     * @param Filter[] $initialFilters
     * @param Closure|null $addJoinsFn
     * @param Closure|null $formatDataFn
     * @param Closure|null $getChildrenFn
     * @param Closure|null $handleDataFn
     */
    public function __construct(ResultClass $resultClass, array $allowedFilterFields, array $allowedSortingFields, array $globalQueryFields, array $initialFilters = [], ?Closure $addJoinsFn = null, ?Closure $formatDataFn = null, ?Closure $getChildrenFn = null, ?Closure $handleDataFn = null) {
        $this->resultClass = $resultClass;
        $this->allowedFilterFields = $allowedFilterFields;
        $this->allowedSortingFields = $allowedSortingFields;
        $this->globalQueryFields = $globalQueryFields;
        $this->initialFilters = $initialFilters;
        $this->addJoinsFn = $addJoinsFn;
        $this->formatDataFn = $formatDataFn;
        $this->getChildrenFn = $getChildrenFn;
        $this->handleDataFn = $handleDataFn;
    }

    /**
     * @throws InvalidConfigException
     */
    public function validate(Request $request): void {
        foreach ($request->sorting as $sorting) {
            foreach ($this->allowedSortingFields as $field) {
                if ($sorting->field == $field->inApi) {
                    continue 2;
                }
            }
            throw new InvalidConfigException('sorting', $sorting->field);
        }
        foreach ($request->filters as $filter) {
            foreach ($this->allowedFilterFields as $field) {
                if ($filter->field == $field->inApi) {
                    continue 2;
                }
            }
            throw new InvalidConfigException('filters', $filter->field);
        }
    }

    /**
     * @return Closure|null
     */
    public function getAddJoinsFn(): ?Closure {
        return $this->addJoinsFn;
    }

    /**
     * @return Closure|null
     */
    public function getFormatDataFn(): ?Closure {
        return $this->formatDataFn;
    }

    /**
     * @return Closure|null
     */
    public function getHandleDataFn(): ?Closure
    {
        return $this->handleDataFn;
    }

    /**
     * @return ResultClass
     */
    public function getResultClass(): ResultClass {
        return $this->resultClass;
    }

    /**
     * @return Field[]
     */
    public function getAllowedFilterFields(): array {
        return $this->allowedFilterFields;
    }

    /**
     * @return Field[]
     */
    public function getAllowedSortingFields(): array {
        return $this->allowedSortingFields;
    }

    /**
     * @return string[]
     */
    public function getGlobalQueryFields(): array {
        return $this->globalQueryFields;
    }

    /**
     * @return array
     */
    public function getInitialFilters(): array {
        return $this->initialFilters;
    }

    public function getQueryFilterField(string $apiField): ?string {
        foreach ($this->allowedFilterFields as $field) {
            if ($field->inApi === $apiField) {
                return $field->inQuery;
            }
        }
        return null;
    }

    /**
     * @param string $apiField
     * @return SortingConfig[]|null
     */
    public function getQuerySortingField(string $apiField): ?array {
        foreach ($this->allowedSortingFields as $field) {
            if ($field->inApi === $apiField) {
                return $field->configs;
            }
        }
        return null;
    }

    /**
     * @return Closure|null
     */
    public function getGetChildrenFn(): ?Closure {
        return $this->getChildrenFn;
    }
}