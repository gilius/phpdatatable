<?php

namespace Datatable;

use Json\JsonField;

class Field {
    public string $inQuery;
    #[JsonField]
    public string $inApi;

    /**
     * @param string $inQuery
     * @param string $inApi
     */
    public function __construct(string $inQuery, string $inApi) {
        $this->inQuery = $inQuery;
        $this->inApi = $inApi;
    }
}