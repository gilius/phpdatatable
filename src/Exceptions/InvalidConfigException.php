<?php

namespace Datatable\Exceptions;

use Exception;
use Throwable;

class InvalidConfigException extends Exception {

    private string $param;
    private mixed $value;


    public function __construct($param, mixed $value, string $message = "", int $code = 0, ?Throwable $previous = null) {
        $this->param = $param;
        $this->value = $value;
        parent::__construct($message, $code, $previous);
    }

    public function getExplanation(): string {
        return "Param `$this->param` cannot have value `$this->value`";
    }
}