<?php

namespace Datatable\Exceptions;

use Exception;

class FilterTypeNotImplemented extends Exception {


    public function __construct(string $filterType) {
        parent::__construct("Filter type `$filterType` not implemented");
    }
}