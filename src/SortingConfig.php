<?php

namespace Datatable;

class SortingConfig {
    public string $field;
    public bool $inverted;

    public ?SortingDirection $forcedDirection = null;

    /**
     * @param string $field
     * @param bool $inverted
     * @param SortingDirection|null $forcedDirection
     */
    public function __construct(string $field, bool $inverted = false, ?SortingDirection $forcedDirection = null) {
        $this->field = $field;
        $this->inverted = $inverted;
        $this->forcedDirection = $forcedDirection;
    }
}