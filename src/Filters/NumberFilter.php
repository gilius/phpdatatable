<?php


namespace Datatable\Filters;

use Datatable\Config;
use Datatable\FilterValueInterface;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\QueryBuilder;
use Json\JsonField;

class NumberFilter implements FilterValueInterface {
    const HavingKeyWords = ["count", "sum"];
    #[JsonField]
    public string $value;
    #[JsonField]
    public OperatorEnum $operator;

    function getQueryComparison(string $field, QueryBuilder $qb, Config $config): ?Comparison {
        $paramCount = count($qb->getParameters());
        $parameter = "param$paramCount";

        $qb->setParameter($parameter, $this->value);

        return match ($this->operator) {
            OperatorEnum::Equals => $qb->expr()->eq($field, ":$parameter"),
            OperatorEnum::GreaterEqualsThan => $qb->expr()->gte($field, ":$parameter"),
            OperatorEnum::GreaterThan => $qb->expr()->gt($field, ":$parameter"),
            OperatorEnum::LessEqualsThan => $qb->expr()->lte($field, ":$parameter"),
            OperatorEnum::LessThan => $qb->expr()->lt($field, ":$parameter"),
            OperatorEnum::NotEquals => $qb->expr()->neq($field, ":$parameter"),
        };
    }

    public function isHaving(string $field): bool {
        return !!array_filter(array_map(fn($keyword) => str_starts_with($field, $keyword), self::HavingKeyWords));
    }
}