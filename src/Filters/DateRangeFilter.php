<?php


namespace Datatable\Filters;

use Datatable\Config;
use Datatable\FilterValueInterface;
use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\QueryBuilder;
use Json\JsonField;

class DateRangeFilter implements FilterValueInterface {
    #[JsonField]
    public array $value;

    function getQueryComparison(string $field, QueryBuilder $qb, Config $config): ?Andx {
        if (!$this->value) {
            return null;
        }
        $paramCount = count($qb->getParameters());
        $startDate = "param$paramCount";
        $paramCount++;
        $endDate = "param$paramCount";

        $exp = $qb->expr()->andX(
            $qb->expr()->gte($field, ":$startDate"),
            $qb->expr()->lte($field, ":$endDate")
        );

        $qb->setParameter($startDate, $this->value[0]);
        $qb->setParameter($endDate, $this->value[1]);

        return $exp;
    }

    public function isHaving(string $field): bool {
        return false;
    }
}