<?php


namespace Datatable\Filters;

use Datatable\Config;
use Datatable\FilterValueInterface;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\QueryBuilder;
use Json\JsonField;

class IdFilter implements FilterValueInterface {
    #[JsonField]
    public string $value;

    function getQueryComparison(string $field, QueryBuilder $qb, Config $config): ?Comparison {
        if (!$this->value) {
            return null;
        }
        $paramCount = count($qb->getParameters());
        $parameter = "param$paramCount";

        $exp = $qb->expr()->like($field, ":$parameter");

        $qb->setParameter($parameter, $this->value);

        return $exp;
    }

    public function isHaving(string $field): bool {
        return false;
    }
}