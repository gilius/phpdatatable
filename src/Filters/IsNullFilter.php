<?php


namespace Datatable\Filters;

use Datatable\Config;
use Datatable\FilterValueInterface;
use Doctrine\ORM\QueryBuilder;
use Json\JsonField;

class IsNullFilter implements FilterValueInterface {
    #[JsonField]
    public bool $value;

    function getQueryComparison(string $field, QueryBuilder $qb, Config $config): string {
        return $this->value ? $qb->expr()->isNull($field) : $qb->expr()->isNotNull($field);
    }

    public function isHaving(string $field): bool {
        return false;
    }
}