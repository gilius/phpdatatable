<?php

namespace Datatable\Filters;

enum OperatorEnum: string {
    case Equals = "=";
    case NotEquals = "!=";
    case GreaterThan = ">";
    case GreaterEqualsThan = ">=";
    case LessThan = "<";
    case LessEqualsThan = "<=";
}