<?php


namespace Datatable\Filters;

use Datatable\Config;
use Datatable\FilterValueInterface;
use Doctrine\ORM\Query\Expr\Func;
use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\QueryBuilder;
use Json\JsonField;

class MultipleFilter implements FilterValueInterface {

    #[JsonField]
    public bool $exact = true;
    #[JsonField]
    public array $value = [];

    function getQueryComparison(string $field, QueryBuilder $qb, Config $config): Func|Orx|null {
        if (!$this->value) {
            return null;
        }

        if (!$this->exact) {
            $or = [];
            foreach ($this->value as $value) {
                $paramCount = count($qb->getParameters());
                $parameter = "param$paramCount";

                $or[] = $qb->expr()->like($field, ":$parameter");
                $qb->setParameter($parameter, "%$value%");
            }
            return $qb->expr()->orX(...$or);
        }

        $paramCount = count($qb->getParameters());
        $parameter = "param$paramCount";

        $exp = $qb->expr()->in($field, ":$parameter");
        $qb->setParameter($parameter, $this->value);

        return $exp;
    }

    public function isHaving(string $field): bool {
        return false;
    }
}