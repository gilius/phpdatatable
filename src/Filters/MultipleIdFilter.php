<?php


namespace Datatable\Filters;

use Datatable\Config;
use Datatable\FilterValueInterface;
use Doctrine\ORM\Query\Expr\Func;
use Doctrine\ORM\QueryBuilder;
use Json\JsonField;

class MultipleIdFilter implements FilterValueInterface {
    #[JsonField]
    public array $value;

    function getQueryComparison(string $field, QueryBuilder $qb, Config $config): ?Func {
        if (!$this->value) {
            return null;
        }
        $paramCount = count($qb->getParameters());
        $parameter = "param$paramCount";

        $exp = $qb->expr()->in($field, ":$parameter");

        $qb->setParameter($parameter, $this->value);

        return $exp;
    }

    public function isHaving(string $field): bool {
        return false;
    }
}