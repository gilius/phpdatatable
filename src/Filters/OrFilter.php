<?php


namespace Datatable\Filters;

use Datatable\Config;
use Datatable\Filter;
use Datatable\FilterJsonDecoder;
use Datatable\FilterValueInterface;
use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\QueryBuilder;
use Json\JsonObjectArray;

class OrFilter implements FilterValueInterface {
    #[JsonObjectArray(className: Filter::class, decoder: new FilterJsonDecoder())]
    public array $value;

    function getQueryComparison(string $field, QueryBuilder $qb, Config $config): ?Orx {
        if (!$this->value) {
            return null;
        }
        return $qb->expr()->orX(
            ...array_map(fn(Filter $x) => $x->getQueryComparison($qb, $config), $this->value)
        );
    }

    public function isHaving(string $field): bool {
        return false;
    }
}