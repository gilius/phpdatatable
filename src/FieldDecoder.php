<?php

namespace Datatable;

use Exception;
use Json\JsonDecoderInterface;

class FieldDecoder implements JsonDecoderInterface {
    function encode(mixed $value): mixed {
        if ($value instanceof Field || $value instanceof SortingField) {
            return $value->inApi;
        }
        return null;
    }

    /**
     * @throws Exception
     */
    function decode(mixed $value): mixed {
        throw new Exception("Not implemented");
    }
}