<?php

namespace Datatable;

use Json\JsonField;
use Json\JsonObjectArray;

class Request {
    #[JsonField]
    public string $query = "";

    #[JsonField]
    public int $page = 1;

    #[JsonField]
    public int $pageSize = 25;

    /** @var Filter[] */
    #[JsonObjectArray(decoder: new FilterJsonDecoder())]
    public array $filters = [];

    /** @var Sorting[] */
    #[JsonObjectArray(className: Sorting::class)]
    public array $sorting = [];

    public function getFirstResult(): int {
        return ($this->page - 1) * $this->pageSize;
    }

    public function getMaxResults(): int {
        return $this->pageSize;
    }
}