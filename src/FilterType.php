<?php


namespace Datatable;

use Datatable\Filters\AndFilter;
use Datatable\Filters\DateRangeFilter;
use Datatable\Filters\IdFilter;
use Datatable\Filters\IsNullFilter;
use Datatable\Filters\MultipleFilter;
use Datatable\Filters\MultipleIdFilter;
use Datatable\Filters\NumberFilter;
use Datatable\Filters\OrFilter;
use Datatable\Filters\TextFilter;
use Json\ClassMapperEnumeratorInterface;

enum FilterType: string implements ClassMapperEnumeratorInterface {
    case Text = "text";
    case Id = "id";
    case Number = "number";

    case MultipleId = "multipleId";
    case MultipleText = "multipleText";
    case MultipleNumber = "multipleNumber";
    case DateRange = "dateRange";
    case IsNull = "isNull";
    case OR = "or";
    case AND = "and";

    function getClass(): string {
        return match ($this) {
            self::Text => TextFilter::class,
            self::Id => IdFilter::class,
            self::MultipleId => MultipleIdFilter::class,
            self::MultipleText, self::MultipleNumber => MultipleFilter::class,
            self::Number => NumberFilter::class,
            self::IsNull => IsNullFilter::class,
            self::DateRange => DateRangeFilter::class,
            self::OR => OrFilter::class,
            self::AND => AndFilter::class,
        };
    }
}