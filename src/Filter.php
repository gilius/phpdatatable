<?php

namespace Datatable;


use Doctrine\ORM\Query\Expr\Base;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\Query\Expr\Func;
use Doctrine\ORM\QueryBuilder;
use Json\JsonField;

class Filter {
    #[JsonField]
    public FilterType $type;
    #[JsonField]
    public string $field;

    //This is custom Json encoded/decoded (using FilterJsonDecoder class) - no attribute needed
    public FilterValueInterface $data;

    function getQueryComparison(QueryBuilder $qb, Config $config, $fieldFromApi = true): null|Comparison|Func|Base|string {
        return $this->data->getQueryComparison($fieldFromApi ? $config->getQueryFilterField($this->field) : $this->field, $qb, $config);
    }

    function isHaving(Config $config, $fieldFromApi = true): bool {
        return $this->data->isHaving($fieldFromApi ? $config->getQueryFilterField($this->field) : $this->field);
    }
}